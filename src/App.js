import React, { Component } from 'react';
import './App.css';

import ReactTable from 'react-table';
import 'react-table/react-table.css';

import Header from './components/header/Header.js';
import FormHome from './components/formHome/FormHome.js';
import Advantages from './components/advantages/Advantages.js';
import Carousel from './components/carousel/Carousel.js';
import Marketing from './components/marketing/Marketing.js';
import Footer from './components/footer/Footer.js';

import RangeSlider from './components/rangeSlider/rangeSlider.js';

import { Circle } from 'rc-progress';


import partnerFerratum from './img/partners_ferratum.png'
import partnerVivus from './img/partners_vivus.png'
import partnerKredito24 from './img/partners_kredito24.png'
import partnerCreditocajero from './img/partners_creditocajero.png'
import partnerCcloan from './img/partners_ccloan.png'
import partnerSavso from './img/partners_savso.png'


class Example extends Component {
  constructor(props) {
    super(props);
    const circleContainerStyle = {
      position: 'relative',
      width: '76px',
      height: '76px',
      display: 'block',
    };
    const circleTextStyle = {
      position: 'absolute',
      display: 'block',
      width: '100%',
      fontSize: '16px',
      textAlign: 'center',
      top: '30px',
      color: '#626e71',
    };
    this.state = {
      percent: props.percent,
      circleContainerStyle: circleContainerStyle,
      circleTextStyle: circleTextStyle,
    };
  }
  render() {
    const colorList = [
      '#ff0300',
      '#ff2a00',
      '#ff4200',
      '#ce5810',
      '#ff9c1b',
      '#ffd106',
      '#ffde4d',
      '#ffeb3f',
      '#b3ef53',
      '#8cdb10'
    ];
    var currentItemColor = colorList[Number(this.props.percent/10-1).toFixed(0)];
    return (
      <div>
        <div style={this.state.circleContainerStyle}>
          <Circle
            percent={this.props.percent}
            strokeWidth="5"
            strokeLinecap="round"
            strokeColor={currentItemColor}
            trailWidth="5"
            trailColor="#f0f4f5"
          />
          <p style={this.state.circleTextStyle}>{this.props.percent}%</p>
        </div>
      </div>
    );
  }
}





class ExtendTableLine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: props.description
    };
  }
  render() {
    var itemDescription = this.props.description;
    var itemDescriptionString = '';
    console.log('RENDER:', itemDescription);
    for (var key in itemDescription) {
      itemDescriptionString = itemDescriptionString + '<li>' + itemDescription[key] +'</li>';
    }
    function createMarkup() { return {__html: itemDescriptionString}; };
    return (
      <div className="result-table-extended">
        <div className="result-table-extended-content">
          <h4 className="result-table-extended-title">Requisitos necesarios</h4>
          <ul className="result-table-extended-list">
            <div dangerouslySetInnerHTML={createMarkup()}></div>
          </ul>
        </div>
        <div className="result-table-extended-sidebar">
          <button className="result-table-extended-btn">Más detalles</button>
        </div>
      </div>
    );
  }
}



export default class App extends Component {

  constructor(props) {
    super(props);
    this.getRangeSliderValues = this.getRangeSliderValues.bind(this);

    this.getFormValues = this.getFormValues.bind(this);
    // Устанавливаем состояние
    this.state = {
      amount: '',
      period: '',
      name: '', 
      surname: '',
      email: '',
      phone: '',
      formIsValidated: '',
      loaderIsShowing: '',
      formIsShowing: ''
    };
  }

  // Пишем в state значения полученные от компонента формы
  getFormValues(value, inputName) {
    var valueObject = {};
    valueObject[inputName] = value;
    this.setState(valueObject);
  }

  // Пишем в state значения полученные от компонента слайдера суммы и срока
  getRangeSliderValues(event, value) {
    var valueObject = {};
    valueObject[event] = value;
    this.setState(valueObject);
  }

  componentDidUpdate(prevProps, prevState) {
    // console.log('COMPONENT DID UPDATE:', prevState.amount, this.state.amount);
  }



  render() {

    const data = [{
        rating: '96',
        name: 'ferratum',
        domain: 'ferratum.es',
        logo_url: partnerFerratum,
        redirect_url: 'https://leadsor.co.uk/',
        amount: '200 €',
        comission: '90 €',
        period: '3 meses',
        period_days: 90,
        total: '379.97 €',
        button: '',
        description: {
          description_item_1: '1Para clientes nuevos el límite para la primera solicitud es de 300€.',
          description_item_2: 'Los clientes actuales pueden pedir prestado hasta 600€.',
          description_item_3: 'En solo unos minutos su Dinero en Efectivo. No importa Asnef.',
          description_item_4: 'Transferencia a la cuenta bancaria del prestatario que ha facilitado en la solicitud de préstamo.',
          description_item_5: 'El prestatario efectuara la cancelación del préstamo en un solo pago en la fecha de vencimiento del mismo.',
          description_item_6: 'Transferencia a la cuenta bancaria del prestatario que ha facilitado en la solicitud de préstamo.'
       },
      },{
        rating: '92',
        name: 'vivus',
        domain: 'vivus.es',
        logo_url: partnerVivus,
        redirect_url: 'https://leadsor.co.uk/',
        amount: '300 €',
        comission: '80 €',
        period: '30 días',
        period_days: 30,
        total: '380.23 €',
        button: '',
        description: {
          description_item_1: '2Para clientes nuevos el límite para la primera solicitud es de 300€.',
          description_item_2: 'Los clientes actuales pueden pedir prestado hasta 600€.',
          description_item_3: 'En solo unos minutos su Dinero en Efectivo. No importa Asnef.',
          description_item_4: 'Transferencia a la cuenta bancaria del prestatario que ha facilitado en la solicitud de préstamo.',
          description_item_5: 'El prestatario efectuara la cancelación del préstamo en un solo pago en la fecha de vencimiento del mismo.',
          description_item_6: 'Transferencia a la cuenta bancaria del prestatario que ha facilitado en la solicitud de préstamo.'
       },
      },{
        rating: '90',
        name: 'kredito24',
        domain: 'kredito24.es',
        logo_url: partnerKredito24,
        redirect_url: 'https://leadsor.co.uk/',
        amount: '250 €',
        comission: '60 €',
        period: '60 días',
        period_days: 60,
        total: '310.23 €',
        button: '',
        description: {
          description_item_1: '3Para clientes nuevos el límite para la primera solicitud es de 300€.',
          description_item_2: 'Los clientes actuales pueden pedir prestado hasta 600€.',
          description_item_3: 'En solo unos minutos su Dinero en Efectivo. No importa Asnef.',
          description_item_4: 'Transferencia a la cuenta bancaria del prestatario que ha facilitado en la solicitud de préstamo.',
          description_item_5: 'El prestatario efectuara la cancelación del préstamo en un solo pago en la fecha de vencimiento del mismo.',
          description_item_6: 'Transferencia a la cuenta bancaria del prestatario que ha facilitado en la solicitud de préstamo.'
       },
      },{
        rating: '89',
        name: 'creditocajero',
        domain: 'creditocajero.es',
        logo_url: partnerCreditocajero,
        redirect_url: 'https://leadsor.co.uk/',
        amount: '240 €',
        comission: '60 €',
        period: '3 meses',
        period_days: 90,
        total: '300.23 €',
        button: '',
        description: {
          description_item_1: '4Para clientes nuevos el límite para la primera solicitud es de 300€.',
          description_item_2: 'Los clientes actuales pueden pedir prestado hasta 600€.',
          description_item_3: 'En solo unos minutos su Dinero en Efectivo. No importa Asnef.',
          description_item_4: 'Transferencia a la cuenta bancaria del prestatario que ha facilitado en la solicitud de préstamo.',
          description_item_5: 'El prestatario efectuara la cancelación del préstamo en un solo pago en la fecha de vencimiento del mismo.',
          description_item_6: 'Transferencia a la cuenta bancaria del prestatario que ha facilitado en la solicitud de préstamo.'
       },
      },{
        rating: '85',
        name: 'ccloan',
        domain: 'ccloan.es',
        logo_url: partnerCcloan,
        redirect_url: 'https://leadsor.co.uk/',
        amount: '260 €',
        comission: '70 €',
        period: '18 días',
        period_days: 18,
        total: '330.23 €',
        button: '',
        description: {
          description_item_1: '5Para clientes nuevos el límite para la primera solicitud es de 300€.',
          description_item_2: 'Los clientes actuales pueden pedir prestado hasta 600€.',
          description_item_3: 'En solo unos minutos su Dinero en Efectivo. No importa Asnef.',
          description_item_4: 'Transferencia a la cuenta bancaria del prestatario que ha facilitado en la solicitud de préstamo.',
          description_item_5: 'El prestatario efectuara la cancelación del préstamo en un solo pago en la fecha de vencimiento del mismo.',
          description_item_6: 'Transferencia a la cuenta bancaria del prestatario que ha facilitado en la solicitud de préstamo.'
       },
      },{
        rating: '81',
        name: 'savso',
        domain: 'savso.es',
        logo_url: partnerSavso,
        redirect_url: 'https://leadsor.co.uk/',
        amount: '290 €',
        comission: '90 €',
        period: '18 días',
        period_days: 18,
        total: '380.23 €',
        button: '',
        description: {
          description_item_1: '6Para clientes nuevos el límite para la primera solicitud es de 300€.',
          description_item_2: 'Los clientes actuales pueden pedir prestado hasta 600€.',
          description_item_3: 'En solo unos minutos su Dinero en Efectivo. No importa Asnef.',
          description_item_4: 'Transferencia a la cuenta bancaria del prestatario que ha facilitado en la solicitud de préstamo.',
          description_item_5: 'El prestatario efectuara la cancelación del préstamo en un solo pago en la fecha de vencimiento del mismo.',
          description_item_6: 'Transferencia a la cuenta bancaria del prestatario que ha facilitado en la solicitud de préstamo.'
       },
      }
    ]
   
    const columns = [
      {
        id: 'ratingGraph',
        Header: 'Probabilidad de aprobación',
        accessor: props => <Example percent={props.rating} />
      }, 
      {
        id: 'partnerName',
        Header: 'Nombre de la empresa',
        accessor: props => <div className="partner"><a href={props.redirect_url}><img width="80px" src={props.logo_url} alt={props.name} /></a><p>{props.domain}</p></div>
      }, 
      {
        Header: 'Importe',
        accessor: 'amount'
      }, 
      {
        Header: 'Comisión',
        accessor: 'comission'
      }, 
      {
        id: 'periodValue',
        Header: 'Plazo',
        accessor: props => <div attrs={props.period_days}>{props.period}</div>,
        sortMethod: (a, b) => {
          return a.props.attrs > b.props.attrs ? 1 : -1;
        }
      }, 
      {
        Header: 'Total a pagar',
        accessor: 'total'
      }, 
      {
        id: 'tableBtn',
        Header: props => <span className="table-last-column"></span>,
        accessor: props => <button className="btn">Elegir</button>,
      }, 
      // {
      //   id: 'tableExt',
      //   Header: props => <span className="table-ext-content"></span>,
      //   accessor: props => <ul><li>1</li><li>2</li></ul>,
      // }, 

    ]


    function onRowClick(state, rowInfo, column, instance) {
        return {
            onClick: e => {
                // console.log('A Td Element was clicked!')
                // console.log('it produced this event:', e)
                // console.log('It was in this column:', column)
                // console.log('It was in this row:', rowInfo)
                // console.log('It was in this table instance:', instance)
            }
        }
    }

    function onExpandedChange(newExpanded, index, event) {
        return {
            onClick: e => {
                // console.log('EXPANDED!!')
                // console.log('it newExpanded:', newExpanded)
                // console.log('It index:', index)
                // console.log('It event:', event)
            }
        }
    }


    // const item_content = data;

    return (
      <div className="App">
        <Header />
        <div className="form-home">
          <div className="wrapper">

            <div className={this.state.formIsValidated ? 'form-home-wrapper fullwidth' : 'form-home-wrapper'}>
              
              <div className={this.state.formIsValidated ? 'form-home-header-color fullwidth' : 'form-home-header-color'}></div>
              <div className={this.state.formIsValidated ? 'form-home-body-color fullwidth' : 'form-home-body-color'}></div>

              <div className={this.state.formIsValidated ? 'form-home-bg hide' : 'form-home-bg'}></div>
              
              <RangeSlider mode={this.state.formIsValidated ? 'fullwidth' : ''} onChange={this.getRangeSliderValues} />
              
              <div className={this.state.formIsShowing ? 'form-home-data-wrapper' : 'form-home-data-wrapper hide'}>
                <FormHome props={this.state} onChange={this.getFormValues} />
              </div>

            </div>

            <div className={this.state.loaderIsShowing ? 'result-table' : 'result-table hide'}>

              <h2 className="result-table-title">Todos los resultados</h2>
             
              <ReactTable
                data={data}
                columns={columns}
                defaultPageSize={6}
                sortable={true}
                multiSort={true}
                resizable={false}
                showPagination={false}
                getTrProps={onRowClick}
                onExpandedChange={(newExpanded, index, event) => { console.log('EXPA!', newExpanded, index, event); }}
                SubComponent={row => {
                  return (
                    <ExtendTableLine index={row.index} description={data[row.index].description} />
                  );
                }}
              />

              

            </div>

          </div>
          <div className={this.state.formIsValidated ? 'form-home-wrapper-bg' : 'form-home-wrapper-bg hide'}></div>
        </div>

        <div className={this.state.formIsValidated ? 'advantages-wrapper hide' : 'advantages-wrapper'}>
          <Advantages />
        </div>

        <Carousel />
        
        <div className={this.state.formIsValidated ? 'marketing-wrapper hide' : 'marketing-wrapper'}>
          <Marketing />
        </div>

        <Footer />
      </div>
    );
  }

}

