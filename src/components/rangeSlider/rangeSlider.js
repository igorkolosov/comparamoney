import React, { Component } from 'react';

import './rangeSlider.css';

import Slider from 'rc-slider';

import 'rc-slider/assets/index.css';


// DEFAULT VALUES
var amountDefault = 10;
var periodDefault = 30;
var currencyValue = '€';
var periodValue = 'días';


// CALCULATING AMOUT VALUES FOR RANGE SLIDER
var amountPoints = [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000];
var periodPoints = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 61, 90, 120, 150, 180, 210, 240, 270, 300, 330, 360, 540, 720];
// function for calculating object with values of range slider bar points
function makeRangeSliderValues(array) {
  var amountMarksArray = [{0: 0}];
  var amountMarksObj = {};
  // building array with calculated step 
  for (var i = 0; i < array.length; i++) {
    var pointObj = {};
    pointObj[i*100/array.length+(100/array.length)] = array[i];
    amountMarksArray.push(pointObj);
  }
  // building object form amount arrow with key as new step value, and value as normal value
  for (var k = 0; k < amountMarksArray.length; k++) {
    for (var j in amountMarksArray[k]) {
      amountMarksObj[Number(j)] = amountMarksArray[k][j];
    }
  }
  return amountMarksObj;
}
// getting values for amounts
var amountMarksObj = makeRangeSliderValues(amountPoints);
var periodMarksObj = makeRangeSliderValues(periodPoints);



class rangeSlider extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      amountVolume: amountPoints[amountDefault],
      periodVolume: periodPoints[periodDefault],
      periodKindValue: 'días',
      mode: '',
    }
  }

  handleOnAmountChange = (value) => {
    // Пробрасываем значение суммы выше в форму
    this.props.onChange('amount', amountMarksObj[value])

    this.setState({
      amountVolume: amountMarksObj[value]
    })
  }

  handleOnPeriodChange = (value) => {

    // Пробрасываем значение периода выше в форму
    this.props.onChange('period', periodMarksObj[value])

    if (periodMarksObj[value] <= 1) {
      this.setState({
        periodVolume: periodMarksObj[value],
        periodKindValue: 'día'
      })
    }
    else if (periodMarksObj[value] < 90) {
      this.setState({
        periodVolume: periodMarksObj[value],
        periodKindValue: 'días'
      })
    } else {
      this.setState({
        periodVolume: periodMarksObj[value]/30,
        periodKindValue: 'meses'
      })
    }
    
  }

  // Пробрасываем значения по умолчанию в родительский элемент
  componentDidMount = (event) => {
    // console.log('componentDidMount!!!!!!', this.props.mode);
    this.props.onChange('amount', amountPoints[amountDefault]);
    this.props.onChange('period', periodPoints[periodDefault]);
  }

  componentDidUpdate = (event, prevEvent) => {
    // console.log('componentDidMount!!!!!!', this.props.mode, prevEvent.mode);
    if (this.props.mode !== prevEvent.mode) this.setState({'mode': this.props.mode})
  }

  render() {
    return (
      <div className="rangeslider-wrapper">
        <div className="rangeslider-item">
          <h3 className="rangeslider-item-title">Importe</h3>
          <Slider
            marks={amountMarksObj}
            dots={false}
            defaultValue={amountDefault*100/amountPoints.length+(100/amountPoints.length)}
            min={100/amountPoints.length}
            max={100}
            step={null}
            tooltip={false}
            orientation="horizontal"
            onChange={this.handleOnAmountChange}
          />
          <p className="rangeslider-item-value">{this.state.amountVolume} {currencyValue}</p>
        </div>
        <div className="rangeslider-item">
          <h3 className="rangeslider-item-title">Plazo</h3>
          <Slider
            marks={periodMarksObj}
            dots={false}
            defaultValue={periodDefault*100/periodPoints.length+(100/periodPoints.length)}
            min={100/periodPoints.length}
            max={100}
            step={null}
            // value={periodVolume}
            tooltip={false}
            orientation="horizontal"
            onChange={this.handleOnPeriodChange}
          />
          <p className="rangeslider-item-value">{this.state.periodVolume} {this.state.periodKindValue}</p>
        </div>
        <div className={this.state.mode==='fullwidth' ? 'rangeslider-button-wrapper visible' : 'rangeslider-button-wrapper'}>
          <button>
            Elegir
          </button>
        </div>
      </div>
    )
  }
}

export default rangeSlider;
