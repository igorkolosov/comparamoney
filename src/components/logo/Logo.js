import React, { Component } from 'react';
// import logo from './logo.svg';
import './Logo.css';

class Logo extends Component {
  render() {
    return (
      <a href="/"
        className="logo">
        <div className="logo-wrap">
          <div className="logo-b"></div>
          <div className="logo-m"></div>
          <div className="logo-point"></div>
        </div>
        <p className="logo-text">
          <span className="logo-text-word logo-text-word-bold">Compara</span>
          <span className="logo-text-word">Money</span>
        </p>
      </a>
    );
  }
}

export default Logo;
