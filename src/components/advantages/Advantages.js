import React, { Component } from 'react';
import './Advantages.css';


class Advantages extends Component {
  render() {
    return (
      <section className="advantages">
        <div className="wrapper">
          
          <div className="advantages-content">

            <h3 className="advantages-content-title">Ventajas:</h3>

            <ul className="advantages-content-list">
              <li className="list-item list-item--1">
                <div className="list-item-icon"></div>
                <div className="list-item-point"></div>
                <div className="list-item-text">
                  <h4 className="list-item-text-title">Cómodo</h4>
                  <p className="list-item-text-paragraph">Usted mismo elije la oferta más ventajosa entre todas las compañías de crédito</p>
                </div>

              </li>
              <li className="list-item list-item--2">
                <div className="list-item-icon"></div>
                <div className="list-item-point"></div>
                <div className="list-item-text">
                  <h4 className="list-item-text-title">Fiable</h4>
                  <p className="list-item-text-paragraph">Trabajamos sólo con compañías fiables que ya han concedido créditos anteriormente</p>
                </div>
              </li>
              <li className="list-item list-item--3">
                <div className="list-item-icon"></div>
                <div className="list-item-point"></div>
                <div className="list-item-text">
                  <h4 className="list-item-text-title">Rápido</h4>
                  <p className="list-item-text-paragraph">No tendrá que desplazarse: podrá solicitar el crédito on-line y el dinero será enviado a su cuenta</p>
                </div>
              </li>
            </ul>



          </div>

          <a href="#" className="advantages-banner">

            <div className="advantages-banner-text">
              <h3 className="advantages-banner-title">Las mejores</h3>
              <p className="advantages-banner-text-paragraph">ofertas de crédito</p>
            </div>

            <div className="advantages-banner-btn">Realizar la solicitud</div>

          </a>

        </div>
      </section>
    );
  }
}

export default Advantages;
