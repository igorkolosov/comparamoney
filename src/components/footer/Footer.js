import React, { Component } from 'react';
// import logo from './logo.svg';
import './Footer.css';

import Logo from '../logo/Logo.js';

class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <div className="wrapper">
          <div className="footer-logo">
            <Logo />
          </div>
          <div className="footer-copyrights">
            <p className="footer-copyrights-text">© ComparaMoney Ltd 2017</p>
          </div>
          <div className="footer-separator">
            <div className="footer-separator-circle"></div>
            <div className="footer-separator-line"></div>
          </div>
          <nav className="footer-nav">
            <a href="/" className="footer-nav-link">
              <p className="footer-nav-text">Quienes somos</p>
            </a>
            <a href="/" className="footer-nav-link">
              <p className="footer-nav-text">Información para socios potenciales</p>
            </a>
          </nav>
          <div className="footer-social">

          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
