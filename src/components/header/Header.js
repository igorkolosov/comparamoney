import React, { Component } from 'react';
// import logo from './logo.svg';
import './Header.css';

import Logo from '../logo/Logo.js';

class Header extends Component {
  render() {
    return (
      <header className="header">
        <div className="wrapper">
          <div className="header-logo">
            <Logo />
          </div>
          <nav className="header-nav">
            <a href="/" className="header-nav-link">
              <p className="header-nav-text">Home</p>
            </a>
            <a href="/" className="header-nav-link">
              <p className="header-nav-text">Contacts</p>
            </a>
          </nav>
        </div>
      </header>
    );
  }
}

export default Header;
