import React, { Component } from 'react';
import Slider from 'react-slick';

import './Carousel.css';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import partnerAdelanta from './../../img/partners_adelanta.png'
import partnerAfluenta from './../../img/partners_afluenta.png'
import partnerAspiria from './../../img/partners_aspiria.png'
import partnerCashper from './../../img/partners_cashper.png'
import partnerCohete from './../../img/partners_cohete.png'
import partnerConlana from './../../img/partners_conlana.png'
import partnerContante from './../../img/partners_contante.png'
import partnerCredijusto from './../../img/partners_credijusto.png'
// import partnerAfluenta from './../../img/partners_afluenta.png'
// import partnerAspiria from './../../img/partners_aspiria.png'
// import partnerCashper from './../../img/partners_cashper.png'
// import partnerCohete from './../../img/partners_cohete.png'
// import partnerConlana from './../../img/partners_conlana.png'
// import partnerContante from './../../img/partners_contante.png'
// import partnerCredijusto from './../../img/partners_credijusto.png'


class Carousel extends Component {
  render() {
    var settings = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 5,
      slidesToScroll: 5,
      centerPadding: '40px'
    };
    return (
      <section className="carousel">
        <div className="wrapper">
          <Slider {...settings}>
            <div><img src={partnerAdelanta} alt='' /></div>
            <div><img src={partnerAfluenta} alt='Afluenta' /></div>
            <div><img src={partnerAspiria} alt='Aspiria' /></div>
            <div><img src={partnerCashper} alt='Cashper' /></div>
            <div><img src={partnerCohete} alt='Cohete' /></div>
            <div><img src={partnerConlana} alt='Conlana' /></div>
            <div><img src={partnerContante} alt='Contante' /></div>
            <div><img src={partnerCredijusto} alt='Credijusto' /></div>
          </Slider>
        </div>
      </section>
    );
  }
}

export default Carousel;
