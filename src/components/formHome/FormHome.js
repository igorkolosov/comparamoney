import React, { Component } from 'react';
import Validator from 'email-validator';
import InputMask from 'react-input-mask';
import { isValidNumber } from 'libphonenumber-js';

import './FormHome.css';

function isPhoneValid(phone) {
  var phoneMask = '+34999999999';
  if (isValidNumber({ phone: phone, country: 'ES' }) && phone.replace(/\s*/g,'').length === phoneMask.length) {
    return true;
  } else {
    return false;
  }
}

class homeForm extends Component {

  constructor(props, context) {
    super(props, context)
    this.state = {
      nameIsEmpty: true,
      surnameIsEmpty: true,
      emailIsEmpty: true,
      phoneIsEmpty: true,
      emailIsValid: false,
      phoneIsValid: false,
      nameHaveErrors: false,
      surnameHaveErrors: false,
      emailHaveErrors: false,
      phoneHaveErrors: false,
      nameValue: '', 
      surnameValue: '',
      emailValue: '',
      phoneValue: '',
      formIsValid: false,
      formIsShowing: true,
      loaderIsShowing: false,
    }
  }


  componentDidMount = (event) => {
    // Пробразываем состояния в App state при инициализации компонента
    this.props.onChange(this.state.nameValue, 'name')
    this.props.onChange(this.state.surnameValue, 'surname')
    this.props.onChange(this.state.emailValue, 'email')
    this.props.onChange(this.state.phoneValue, 'phone')
    this.props.onChange(this.state.formIsValid, 'formIsValidated')
    this.props.onChange(this.state.formIsShowing, 'formIsShowing')
    this.props.onChange(this.state.loaderIsShowing, 'loaderIsShowing')
  }

  handleSubmit = (event) => {
    event.preventDefault();

    console.log('submit');

    if (this.state.nameIsEmpty) {
      this.setState({
        nameHaveErrors: true,
      })
    } else {
      this.setState({
        nameHaveErrors: false,
      })
    }
    if (this.state.surnameIsEmpty) {
      this.setState({
        surnameHaveErrors: true,
      })
    } else {
      this.setState({
        surnameHaveErrors: false,
      })
    }
    if (this.state.emailIsEmpty || !this.state.emailIsValid) {
      this.setState({
        emailHaveErrors: true,
      })
    } else {
      this.setState({
        emailHaveErrors: false,
      })
    }
    if (this.state.phoneIsEmpty || !this.state.phoneIsValid) {
      this.setState({
        phoneHaveErrors: true,
      })
    } else {
      this.setState({
        phoneHaveErrors: false,
      })
    }


    if (!this.state.nameIsEmpty && !this.state.surnameIsEmpty && !this.state.emailIsEmpty && !this.state.phoneIsEmpty && this.state.emailIsValid && this.state.phoneIsValid) {

      this.setState({
        formIsValid: true,
        loaderIsShowing: true,
        formIsShowing: false
      })
      // Пробразываем состояния в App state
      this.props.onChange(true, 'formIsValidated')
      this.props.onChange(true, 'loaderIsShowing')
      this.props.onChange(false, 'formIsShowing')

    }

  }

  onFieldChange = function(fieldName, e) {

    var next = {};
    if (e.target.value.trim().length > 0) {
      next[fieldName] = false;
      this.setState(next);
      // NAME INPUT
      if (fieldName === 'nameIsEmpty') {

        this.setState({
          nameValue: e.target.value,
          nameHaveErrors: false
        })
        // Пробразываем значение в App state
        this.props.onChange(e.target.value, e.target.name)
      }
      // SURNAME INPUT
      if (fieldName === 'surnameIsEmpty') {
        this.setState({
          surnameValue: e.target.value,
          surnameHaveErrors: false
        })
        // Пробразываем значение в App state
        this.props.onChange(e.target.value, e.target.name)
      }
      // EMAIL INPUT
      if (fieldName === 'emailIsEmpty') {
        this.setState({
          emailValue: e.target.value,
        })
        // Пробразываем значение в App state
        this.props.onChange(e.target.value, e.target.name)
        if (!Validator.validate(e.target.value)) {
          this.setState({
            emailIsValid: false,
          })
        } else {
          this.setState({
            emailIsValid: true,
            emailHaveErrors: false
          })
        }
      }
      // PHONE INPUT 
      if (fieldName === 'phoneIsEmpty') {
        this.setState({
          phoneValue: e.target.value,
        })
        // Пробразываем значение в App state
        this.props.onChange(e.target.value, e.target.name)
        if (!isPhoneValid(e.target.value)) {
          this.setState({
            phoneIsValid: false,
          })
        } else {
          this.setState({
            phoneIsValid: true,
            phoneHaveErrors: false
          })
        }
      }
     
    } else {
      next[fieldName] = true;
      this.setState(next);
    }
  }

  // После FocusOut из инпутов пробрасываем их значения выше в App (при очистке state в app не менялся)
  onInputBlur = function(name, event) {
    this.props.onChange(event.target.value, event.target.name)
  }

  // // Пробрасываем значения слайдера выше в App
  getRangeSliderValues(event, value) {
    this.props.onChange(value, event)
  }


  render() {
    return (
      <form 
        className="form-home-data" 
        // className={this.state.formIsShowing ? 'form-home-data' : 'form-home-data hide'} 
        onSubmit={this.handleSubmit} 
        noValidate>
        <div className="form-home-data-inputs">
          <div className={this.state.nameHaveErrors ? 'form-group errors' : 'form-group'}>
            <label className="form-label" htmlFor="name">Nombre:</label>
            <input type="text" required 
              className="form-control" 
              name="name"
              placeholder=""
              value={this.state.name}
              onChange={this.onFieldChange.bind(this, 'nameIsEmpty')}
              onBlur={this.onInputBlur.bind(this, 'name')} />
            <p className={this.state.nameIsEmpty ? 'form-error-empty-field visible' : 'form-error-empty-field '}>Поле пустое</p>
          </div>
          <div className={this.state.surnameHaveErrors ? 'form-group errors' : 'form-group'}>
            <label className="form-label" htmlFor="surname">Apellidos:</label>
            <input type="text" required className="form-control" name="surname"
              placeholder=""
              value={this.state.surname}
              onChange={this.onFieldChange.bind(this, 'surnameIsEmpty')}
              onBlur={this.onInputBlur.bind(this, 'surname')} />
            <p className={this.state.surnameIsEmpty ? 'form-error-empty-field visible' : 'form-error-empty-field '}>Поле пустое</p>
          </div>
          <div className={this.state.emailHaveErrors ? 'form-group errors' : 'form-group'}>
            <label className="form-label" htmlFor="email">Correo electrónico:</label>
            <input type="email" required 
              className="form-control" 
              name="email"
              placeholder=""
              value={this.state.email} 
              onChange={this.onFieldChange.bind(this, 'emailIsEmpty')}
              onBlur={this.onInputBlur.bind(this, 'email')} />
            <p className={this.state.emailIsEmpty ? 'form-error-empty-field visible' : 'form-error-empty-field '}>Поле пустое</p>
            <p className={this.state.emailIsValid ? 'form-error' : 'form-error visible'}>Неправильный email</p>
          </div>
          <div className={this.state.phoneHaveErrors ? 'form-group form-group-phone errors' : 'form-group form-group-phone'}>
            <label className="form-label" htmlFor="phone">Teléfono:</label>
            <InputMask type="phone" 
              className="form-control" 
              name="phone"
              placeholder=""
              value={this.state.phone}
              onChange={this.onFieldChange.bind(this, 'phoneIsEmpty')} {...this.state} mask="+3\4 999 999 999" maskChar=" "
              onBlur={this.onInputBlur.bind(this, 'phone')}  />
            <p className={this.state.phoneIsEmpty ? 'form-error-empty-field visible' : 'form-error-empty-field '}>Поле пустое</p>
            <p className={this.state.phoneIsValid ? 'form-error' : 'form-error visible'}>Неправильный телефон</p>
          </div>
        </div>
        <div className="form-home-data-btn-group">
          <div className="form-home-data-point"></div>
          <div className="form-home-data-line"></div>
          <div className="form-home-data-point"></div>
          <div className="form-home-data-line"></div>
          <div className="form-home-data-point"></div>
          <div className="form-home-data-line form-home-data-line--last"></div>
          <button 
            type="submit"
            className="btn btn-submit" 
            >
            Elegir
          </button>
        </div>
      </form>
    )
  }
}

export default homeForm;
