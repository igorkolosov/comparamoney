import React, { Component } from 'react';
import './Marketing.css';

import imgSidebar from './img_sidebar_marketing.png'

class Marketing extends Component {
  render() {
    return (
      <section className="marketing">
        <div className="wrapper">
          
          <div className="marketing-content">

            <h3 className="marketing-content-title">Compara Money</h3>

            <div className="marketing-content-short-text">

              <div className="marketing-content-short-text-icon"></div>

              <p className="marketing-content-short-text-paragraph">¿Usted quiere pedir el crédito y no sabe por dónde empezar? En Internet se pueden encontrar hoy en día decenas de propuestas de préstamo y cada vez es más difícil de entender qué oferta es la mejor. Por eso hemos desarrollado un servicio que le ayudará a encontrar inmediatamente la mejor empresa para resolver fácilmente sus problemas financieros. Todo el proceso es muy simple y rápido, y lo más importante es que todo se hace online.</p>

            </div>

            <div className="marketing-content-long-text">

              <p className="marketing-content-long-text-paragraph">Con nuestro servicio no necesitará visitar todos los sitios web y llamar a las compañías de crédito para obtener más información. Aquí hemos reunido absolutamente toda la información acerca de los préstamos, con lo cual conseguir el dinero será más rápido y cómodo.</p>
              
              <p className="marketing-content-long-text-paragraph">¿Cómo se solicitan los microcréditos?</p>
              
              <p className="marketing-content-long-text-paragraph">El crédito a corto plazo (microcrédito) se puede obtener en su totalidad online sin salir de casa. Por esa razón la comparación de las ofertas y la recepción del dinero a través de nuestro servicio es una tarea simple incluso para las personas no familiarizadas con las finanzas.</p>
              
              <p className="marketing-content-long-text-paragraph">Nuestra base de datos contiene todas las compañías de microcréditos de confianza actuales y ello le permitirá comparar estas empresas y descubrir sus ventajas o inconvenientes, además de poder conocer los requisitos necesarios.</p>
              
              <p className="marketing-content-long-text-paragraph">Todo lo que necesita es seleccionar una empresa que se adapte a usted y rellenar un formulario de solicitud online. Después de este paso en unos 10-15 minutos uno de nuestros agentes se pondrá en contacto con usted para la confirmación de los datos proporcionados y el dinero será transferido a su cuenta.</p>
              
              <p className="marketing-content-long-text-paragraph">Conceptos básicos para elegir un crédito.</p>
              
              <p className="marketing-content-long-text-paragraph">Las opciones que podrá ver como resultado de su solicitud están clasificadas por porcentaje de aprobación. Cuanto mayor sea el porcentaje, más probabilidad tendrá de obtener un préstamo de esta empresa.</p>
              
              <p className="marketing-content-long-text-paragraph">Cada una de las opciones ofrecidas tiene sus propios requisitos para el cliente. Es muy importante prestar atención no sólo a las condiciones del préstamo, sino también a las condiciones de la entidad de crédito hacía el cliente. Leyendo y comparando estos requisitos podrá elegir la opción que más le convenga.</p>
            
            </div>

          </div>

          <div className="marketing-sidebar">

            <h3 className="marketing-sidebar-title">¿Por qué somos los mejores?</h3>

            <img src={imgSidebar} className="marketing-sidebar-img" alt={"¿Por qué somos los mejores?"}/> 

            <p className="marketing-sidebar-paragraph">Nuestra compañía monitorea diariamente el mercado de los créditos no sólo porque quiere ofrecerle la información más fresca y útil, sino porque queremos defender a nuestros potenciales clientes de las empresas de dudosa reputación que últimamente aparecen a menudo en Internet. Dichas compañías ofrecen altas comisiones, poca comunicación y sanciones elevadas. En nuestro sitio web sólo encontrará las mejores propuestas y nunca a los representantes de estas empresas.</p>

            <p className="marketing-sidebar-paragraph">El portal Compara Money es una fuente de información gratuita y tiene el carácter consultivo. Aquí puede encontrar empresas comprobadas y de confianza que ofrecen los préstamos rápidos en el mercado financiero mexicano.</p>

            <p className="marketing-sidebar-paragraph">Con la ayuda de nuestro servicio usted puede encontrar el crédito que necesita con la mejor tasa de interés y con una rápida aprobación. Para mantener la calidad de nuestro portal monitoreamos constantemente el mercado y comprobamos las nuevas empresas. Por ello no tendrá que estudiar las reseñas sobre otras compañías, solamente tendrá que elegir un préstamo y rellenar una solicitud.</p>

          </div>

        </div>
      </section>
    );
  }
}

export default Marketing;
